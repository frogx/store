<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {

	public function daftar()
	{
		$data = array(
			'nama_konsumen' => $this->input->post('namalengkap'),
			'email_konsumen' => $this->input->post('email'),
			'password_konsumen' => md5($this->input->post('password')),
			'no_hp_konsumen' => $this->input->post('nohp')
		);
		$this->db->insert('tbl_konsumen', $data);

		$data_session = array(
			'email_konsumen' => $this->input->post('email'),
			'nama_konsumen' => $this->input->post('namalengkap'),
			'id_konsumen' => $this->db->insert_id()
		);
		
		$this->session->set_userdata( $data_session );
		redirect(site_url(),'refresh');
	}

	public function logout()
	{
		session_destroy();
		redirect(site_url(),'refresh');
	}

	public function login()
	{
		$cek = $this->db->where(array('email_konsumen' => $this->input->post('email'), 'password_konsumen' => md5($this->input->post('password'))))->get('tbl_konsumen')->result();
		if ($cek) {
			$data_session = array(
				'email_konsumen' => $cek[0]->email_konsumen,
				'nama_konsumen' => $cek[0]->nama_konsumen,
				'id_konsumen' => $cek[0]->id_konsumen
			);
			$this->session->set_userdata( $data_session );
			redirect(site_url(),'refresh');
		}else{
			echo '<script language="javascript">';
			echo 'alert("Mohon Maaf, USERNAMA ATAU PASSWORD SALAH !!!!")';
			echo '</script>';
			echo '<script type="text/javascript">';    
			echo 'window.location.assign("'.site_url().'")'; 
			echo '</script>';
		}
	}

	public function pesanan()
	{
		$data['pesanan'] =  $this->db->where('id_konsumen', $this->session->userdata('id_konsumen'))->get('tbl_pesanan')->result();
		$tmp['content'] = $this->load->view('pesanan',$data, true);
		$this->load->view('template', $tmp);
	}

}

/* End of file Akun.php */
/* Location: ./application/controllers/Akun.php */