<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$data['content'] = $this->load->view('admin/home','',true);
		$this->load->view('admin/template',$data);
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/admin/Home.php */